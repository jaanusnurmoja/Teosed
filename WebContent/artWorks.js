var allArtWorkList;
var allArtWorkTypesList;
var thisArtWorkHasAuthors;
var workAuthorsJsonId;
var authorRoles;
var authorName;


var jsonData = "http://localhost:8080/teoseregister/rest/works";
var typesJson = "http://localhost:8080/teoseregister/rest/artworktypes";
var workAuthorsJson = "http://localhost:8080/teoseregister/rest/workauthors";
var rolesJson = "http://localhost:8080/teoseregister/rest/roles"
var authorJson = "http://localhost:8080/teoseregister/rest/author";
var cntType = "application/json; charset=utf-8";

function getAllArtWorksTypesList() 
{
    $.getJSON(typesJson, function (allArtWorkTypes) 
    		{
	        allArtWorkTypesList = allArtWorkTypes;
	        console.log(allArtWorkTypes);
	        if (document.getElementById("newTypeID"))
			{
	            var newTypeID = document.getElementById("newTypeID");
	            for (var i = 0; i < allArtWorkTypes.length; i++) 
	            {
	                var option = document.createElement("option");
	                option.value = allArtWorkTypes[i].typeId;
	                option.text = allArtWorkTypes[i].typeName;
	                newTypeID.add(option);
	             }
			}
	        if (document.getElementById("modifyTypeID"))
			{
	            var modifyTypeID = document.getElementById("modifyTypeID");
	            for (var i = 0; i < allArtWorkTypes.length; i++) 
	            {
	                var option = document.createElement("option");
	                option.value = allArtWorkTypes[i].typeId;
	                option.text = allArtWorkTypes[i].typeName;
	                modifyTypeID.add(option);
	             }
			}
	    }
   );
}

getAllArtWorksTypesList();

function getAuthorRoles() 
{
    $.getJSON(rolesJson, function (roles) 
    		{
    		authorRoles = roles;
	        console.log(roles);
	        if (document.getElementById("newRoleId"))
			{
	            var newRoleId = document.getElementById("newRoleId");
	            for (var i = 0; i < authorRoles.length; i++) 
	            {
	                var option = document.createElement("option");
	                option.value = authorRoles[i].roleId;
	                option.text = authorRoles[i].roleName;
	                newRoleId.add(option);
	             }
			}
	        if (document.getElementById("modifyRoleID"))
			{
	            var modifyRoleID = document.getElementById("modifyRoleID");
	            for (var i = 0; i < authorRoles.length; i++) 
	            {
	                var option = document.createElement("option");
	                option.value = authorRoles[i].roleId;
	                option.text = authorRoles[i].roleName;
	                modifyRoleId.add(option);
	             }
			}
	    }
   );
}

getAuthorRoles();

function getAuthorName() 
{
    $.getJSON(authorJson, function (author) 
    		{
    		authorName = author;
            
	        for (var i = 0; i < authorName.length; i++) 
            {
	        	authorName[i].fullName = authorName[i].firstName + " " + authorName[i].lastName;
                
                var option = document.createElement("option");
                option.value = authorName[i].authorId;
                option.text = authorName[i].fullName;            

                if (document.getElementById("newAuthorId"))
				{
		            var newAuthorId = document.getElementById("newAuthorId");
	                newAuthorId.add(option);
				}

	            if (document.getElementById("modifyAuthorId"))
				{
		            var modifyAuthorId = document.getElementById("modifyAuthorId");
	                modifyAuthorId.add(option);
	             }
            }
	        console.log(author);
	    }
   );
}

getAuthorName();

function getAllArtWorksAndFillTable() 
{
    $.getJSON(jsonData, function (allArtWorks) 
    	{
            allArtWorkList = allArtWorks;
            
            // <!-- see siin on tegelikult globaalse muutuja väärtustamine. Globaalset muutujat läheb vaja, et mujal sama Jsonit kasutada -->
            
           
            var tableBodyContent = "";
            var workHasAuthors;
            var thisArtWork;
            
            
            for (var i = 0; i < allArtWorks.length; i++) 
			{
                
            	thisArtWork = allArtWorks[i];
							
				tableBodyContent = tableBodyContent + 
                "<tr><td>" + thisArtWork.artWorkId +
                "</td><td>" + thisArtWork.artWorkName +
                "</td><td><table id='authorsCell" + thisArtWork.artWorkId + "'></table></td><td>" + thisArtWork.artWorkCreationTime +
                "</td><td><button type='button' onclick='deleteArtWork(" + allArtWorks[i].artWorkId + ")'>Delete</button>" +
                "</td><td><button type='button' onclick='modifyArtWork(" + allArtWorks[i].artWorkId + ")'>Modify</button>" +
                "</td></tr>";

          }
           document.getElementById("ArtworkTableBody").innerHTML = tableBodyContent;
	       
           fillTheRest();

    	
    	}
    );
}

           	
getAllArtWorksAndFillTable();

function fillTheRest()
{
    $.getJSON(jsonData, function (allArtWorks) 
        	{
                allArtWorkList = allArtWorks;
               
                var tableBodyContent = "";
                var workHasAuthors;
                var thisArtWork;
                
                allArtWorks.forEach(function(thisArtWork)
                {
                	$.getJSON(workAuthorsJson + "/work/" + thisArtWork.artWorkId, function (thisArtWorkHasAuthors) 
						{
							workHasAuthors = thisArtWorkHasAuthors;
				            var authorsInTableCell;
				            authorsInTableCell = workHasAuthors.map(function(elem){
			            	    return "<tr><td>" + elem.roleName + ": </td>" + "<td>" + elem.authorName + "</td></tr>";
				            	}).join("");
				            
				            console.log(workHasAuthors);
							document.getElementById("authorsCell" + thisArtWork.artWorkId).innerHTML = authorsInTableCell;
        												
        				}
        			);
                }
           );
       }
    );
}

function addNewArtWork() {
    var jsonData = {
        "artWorkName": document.getElementById("newArtWorkName").value,
        "artWorkAuthors": document.getElementById("newArtWorkAuthors").value,
        "artWorkCreationTime": document.getElementById("newArtWorkCreationTime").value,
        "firstPresentation": document.getElementById("newFirstPresentation").value,
        "placeOfFirstPresentation": document.getElementById("newPlaceOfFirstPresentation").value,
        "typeId": document.getElementById("newTypeId").value
  };
    var jsonString = JSON.stringify(jsonData);

    $.ajax({
        url: jsonData,
        type: "POST",
        data: jsonString,
        contentType: cntType,
        success: function () {
            document.getElementById("newArtWorkName").value = "";
            document.getElementById("newArtWorkCreationTime").value = "";
            document.getElementById("newArtWorkAuthors").value = "";
            document.getElementById("newFirstPresentation").value = "";
            document.getElementById("newPlaceOfFirstPresentation").value = "";
            document.getElementById("newTypeId").value = "";
            document.getElementById("modifyArtWorkId").value = "";
            document.getElementById("modifyArtWorkName").value = "";
            document.getElementById("modifyArtCreationTime").value = "";
            document.getElementById("modifyArtWorkAuthors").value = "";
            document.getElementById("modifyFirstPresentation").value = "";
            document.getElementById("modifyPlaceOfFirstPresentation").value = "";
            document.getElementById("modifywTypeId").value = "";
            getAllArtWorksAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new teose tüüp");
        }
    })
}

function deleteArtWork(artWorkId) {
    var jsonData = {
        "artWorkId": "delete/" + artWorkId
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: jsonData,
        type: "DELETE",
        contentType: cntType,
        data: jsonDataString,
        success: function () {
            getAllArtWorksAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on deleting artworktype");
        }
    })


}

function modifyArtWork(artWorkId) {
    for (var i = 0; i < allArtWorkList.length; i++) {
        if (allArtWorkList[i].artWorkId == artWorkId) {
            document.getElementById("modifyArtWorkId").value = artWorkId;
            document.getElementById("modifyArtWorkName").value = allArtWorkList[i].artWorkName;
            document.getElementById("modifyArtCreationTime").value = allArtWorkList[i].artCreationTime;
            document.getElementById("modifyArtWorkAuthors").value = allArtWorkList[i].artWorkAuthors;
            document.getElementById("modifyFirstPresentation").value = allArtWorkList[i].artWorkFirstPresentation;
            document.getElementById("modifyPlaceOfFirstPresentation").value = allArtWorkList[i].PlaceOfFirstPresentation;
        }
    }

}

function saveArtWorkChanges() {
    var jsonData = {
        "artWorkId": document.getElementById("modifyArtWorkId").value,
        "artWorkName": document.getElementById("modifyArtWorkName").value,
        "artCreationTime": document.getElementById("modifyArtCreationTime").value,
    	"artWorkAuthors": document.getElementById("modifyArtWorkAuthors").value,
    	"firstPresentation": document.getElementById("modifyFirstPresentation").value,
    	"placeOfFirstPresentation": document.getElementById("modifyPlaceOfFirstPresentation").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    var urlTxtAwtForChange = jsonData + "/update"
    $.ajax({
        url: urlTxtAwtForChange,
        type: "PUT",
        data: jsonDataString,
        contentType: cntType,
        success: function () {
            document.getElementById("newArtWorkName").value = "";
            document.getElementById("newArtWorkCreationTime").value = "";
            document.getElementById("newArtWorkAuthors").value = "";
            document.getElementById("newFirstPresentation").value = "";
            document.getElementById("newPlaceOfFirstPresentation").value = "";
            document.getElementById("modifyArtWorkId").value = "";
            document.getElementById("modifyArtWorkName").value = "";
            document.getElementById("modifyArtCreationTime").value = "";
            document.getElementById("modifyArtWorkAuthors").value = "";
            document.getElementById("modifyFirstPresentation").value = "";
            document.getElementById("modifyPlaceOfFirstPresentation").value = "";
            getAllArtWorksAndFillTable()
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on modifying teose tüüp")
        }
    })

}

