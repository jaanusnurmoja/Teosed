var allAuthorsList;
var urlText = "http://localhost:8080/teoseregister/rest/proov/author";
var conText = "application/json; charset=utf-8";

function getAllAuthors() {

    $.getJSON(urlText, function (allAuthorData) {
            allAuthorsList = allAuthorData;

            var tableBodyContent = "";
            for (var i = 0; i < allAuthorData.length; i++) {
                tableBodyContent = tableBodyContent +
                    "<tr><td>" + allAuthorData[i].authorId +
                    "</td><td>" + allAuthorData[i].firstName +
                    "</td><td>" + allAuthorData[i].lastName +
                    "</td><td><button type='button' onclick='deleteAuthorsData(" +
                    allAuthorData[i].authorId + ")'>Delete</button>" +
                    "<button type='button' onclick='changeAuthorData(" + allAuthorData[i].authorId +
                    ")'>Modify</button>" +
                    "</td></tr>"
            }
            document.getElementById("AuthorTableBody").innerHTML = tableBodyContent;
        }
    )
}

getAllAuthors();

function addAuthor() {
    var jsonData = {
        "firstName": document.getElementById("newAuthorFirstName").value,
        "lastName": document.getElementById("newAuthorLastName").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlText,
        type: "POST",
        data: jsonDataString,
        contentType: conText,
        success: function () {
            emptyDataLabels();
            getAllAuthors();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new autor");
        }
    })
}

function deleteAuthorsData(authorDataId) {
    var jsonData = {
        "authorId": authorDataId
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlText,
        type: "DELETE",
        data: jsonDataString,
        contentType: conText,
        success: function () {
            emptyDataLabels();
            getAllAuthors();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Error on deleting autor data");
        }
    })
}


function changeAuthorData(authorId) {
    for (var i=0; i<allAuthorsList.length; i++){
        document.getElementById("modifyAutorID").value=authorId;
        document.getElementById("modifyAuthorFirstName").value=allAuthorsList[i].firstName;
        document.getElementById("modifyAuthorLastName").value=allAuthorsList[i].lastName;
    }

}

function saveAuthorDataChanges() {
    var jsonDataUpdate = {
        "authorId" : document.getElementById("modifyAutorID").value,
        "firstName": document.getElementById("modifyAuthorFirstName").value,
        "lastName": document.getElementById("modifyAuthorLastName").value
    };
    var jsonDataUpdateString =JSON.stringify(jsonDataUpdate);
    var urlTextForChanges = urlText + "/" + "authorId";
    $.ajax({
        url: urlTextForChanges,
        type: "PUT",
        data: jsonDataUpdateString,
        contentType: conText,
        success: function () {
            emptyDataLabels();
            getAllAuthors();
            alert("Andmed muudetud")
        }, error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Eroor on modifying data");
        }
    })

}

function emptyDataLabels() {
    document.getElementById("newAuthorFirstName").value = "";
    document.getElementById("newAuthorLastName").value = "";
    document.getElementById("modifyAutorID").value = "";
    document.getElementById("modifyAuthorFirstName").value = "";
    document.getElementById("modifyAuthorLastName").value = "";
}

