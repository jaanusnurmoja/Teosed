var allArtWorkTypesList;
var urlTxtAWT = "http://localhost:8080/teoseregister/rest/artworktypes";
var conTextAWT = "application/json; charset=utf-8";

function getAllArtWorkTypesAndFillTable() {
    $.getJSON(urlTxtAWT, function (allArtWorkTypes) {
            allArtWorkTypesList = allArtWorkTypes;
            
            // <!-- see siin on tegelikult globaalse muutuja väärtustamine. Globaalset muutujat läheb vaja, et mujal sama Jsonit kasutada -->
  
            var tableBodyContent = "";
            for (var i = 0; i < allArtWorkTypes.length; i++) {
                tableBodyContent = tableBodyContent +
                    "<tr><td>" + allArtWorkTypes[i].typeId +
                    "</td><td>" + allArtWorkTypes[i].typeName +
                    "</td><td>" + allArtWorkTypes[i].typeDescription +
                    "</td><td><button type='button' onclick='deleteArtWorkType(" + allArtWorkTypes[i].typeId + ")'>Delete</button>" +
                    "<button type='button' onclick='modifyCourseType(" + allArtWorkTypes[i].typeId + ")'>Modify</button>" +
                    "</td></tr>"
            }
            document.getElementById("ArtworkTypesTableBody").innerHTML = tableBodyContent;
            
            if (document.getElementById("modifyTypeID"))
    		{
	            var dropDownOpts = "";
	            for (var i = 0; i < allArtWorkTypes.length; i++) 
	            {
	            	dropDownOpts = dropDownOpts +
	                    "<option value="+ allArtWorkTypes[i].typeId + ">" + allArtWorkTypes[i].typeName + "</option>"
	             }
	            document.getElementById("modifyTypeID").innerHTML = dropDownOpts;
       	
    		}
    	}
    );
}

getAllArtWorkTypesAndFillTable();

function addNewArtWorkType() {
    var jsonData = {
        "typeDescription": document.getElementById("newArtWorkTypeDescription").value,
        "typeName": document.getElementById("newArtWorkTypeName").value
    };
    var jsonString = JSON.stringify(jsonData);

    $.ajax({
        url: urlTxtAWT,
        type: "POST",
        data: jsonString,
        contentType: conTextAWT,
        success: function () {
            document.getElementById("newArtWorkTypeDescription").value = "";
            document.getElementById("newArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeId").value = "";
            document.getElementById("modifyArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeDescription").value = "";
            getAllArtWorkTypesAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new teose tüüp");
        }
    })
}

function deleteArtWorkType(artWorkTypeId) {
    var jsonData = {
        "typeId": artWorkTypeId
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlTxtAWT,
        type: "DELETE",
        contentType: conTextAWT,
        data: jsonDataString,
        success: function () {
            getAllArtWorkTypesAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on deleting artworktype");
        }
    })


}

function modifyArtWorkType(artWorkTypeId) {
    for (var i = 0; i < allArtWorkTypesList.length; i++) {
        if (allArtWorkTypesList[i].typeId == artWorkTypeId) {
            document.getElementById("modifyArtWorkTypeId").value = artWorkTypeId;
            document.getElementById("modifyArtWorkTypeName").value = allArtWorkTypesList[i].typeName;
            document.getElementById("modifyArtWorkTypeDescription").value = allArtWorkTypesList[i].typeDescription;
        }
    }

}

function saveArtWorkTypeChanges() {
    var jsonData = {
        "typeId": document.getElementById("modifyArtWorkTypeId").value,
        "typeName": document.getElementById("modifyArtWorkTypeName").value,
        "typeDescription": document.getElementById("modifyArtWorkTypeDescription").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    var urlTxtAwtForChange = urlTxtAWT + "/" + "typeId"
    $.ajax({
        url: urlTxtAwtForChange,
        type: "PUT",
        data: jsonDataString,
        contentType: conTextAWT,
        success: function () {
            document.getElementById("newArtWorkTypeDescription").value = "";
            document.getElementById("newArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeId").value = "";
            document.getElementById("modifyArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeDescription").value = "";
            getAllArtWorkTypesAndFillTable()
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on modifying teose tüüp")
        }
    })

}

