package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.teoseregister.dao.Roles;
import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public abstract class RolesResource
{
	public static List<Roles> getAllRoles()
	{
		List<Roles> allRoles = new ArrayList<>();
		String sqlQuery = "SELECT * FROM roles";
		
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
                .executeQuery();)
        { 
        	while (results.next())
        	{
        		Roles role = new Roles();
        		role.setRoleId(results.getInt("role_id"));
        		role.setRoleName(results.getString("role_name"));
        		role.setEayCode(results.getString("eay_code"));
        		allRoles.add(role);
        	}
        } 
        catch (SQLException e) 
        {
            System.out.println("Error on getting all artworks " + e.getMessage());
        }

		return allRoles;
		
	}
    public static Roles addNewRole(Roles newRole) {
        String sqlQuery = "INSERT INTO `roles`" +
                " (`role_name`, `eay_code`) " + "VALUES ('" +
                newRole.getRoleName() + "', '" +
                newRole.getEayCode() + "');";
        try (Statement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                ResultSet resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    newRole.setRoleId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new artwork type");
        }
        return newRole;
    }

    public static boolean updateRole(Roles roleToUpdate) {
        boolean isUpdated = false;
        System.out.println(roleToUpdate);
        String sqlQuery = "UPDATE roles SET role_name='" +
                roleToUpdate.getRoleName() + "', eay_code='" +
                roleToUpdate.getEayCode() + "' WHERE role_id=" +
                roleToUpdate.getRoleId();
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating role type ");
        }
        return isUpdated;
    }

    public static boolean deleteRoleById(int roleId) {
        boolean isDeleted = false;
        String sqlQuery = "DELETE FROM roles WHERE role_id = " + roleId;
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDeleted = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork type ");
        }

        return isDeleted;
    }
}
