package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.teoseregister.dao.Versions;
import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public abstract class VersionsResource
{
	public static List<Versions> getAllVersions()
	{
		List<Versions> allVersions = new ArrayList<>();
		String sqlQuery = "SELECT * FROM versions";
		
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
                .executeQuery();)
        { 
        	while (results.next())
        	{
        		Versions version = new Versions();
        		version.setVersionId(results.getInt("version_id"));
        		version.setFirstArtWorkId(results.getInt("first_artwork_id"));
        		version.setThisArtWorkId(results.getInt("this_artwork_id"));
        		allVersions.add(version);
        	}
        } 
        catch (SQLException e) 
        {
            System.out.println("Error on getting all artworks " + e.getMessage());
        }

		return allVersions;
		
	}
    public static Versions addNewVersion(Versions newVersion) {
        String sqlQuery = "INSERT INTO `versions`" +
                " (`first_artwork_id`, `this_artwork_id`) " + "VALUES ('" +
                newVersion.getFirstArtWorkId() + "', '" +
                newVersion.getThisArtWorkId() + "');";
        try (Statement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                ResultSet resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    newVersion.setVersionId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new artwork type");
        }
        return newVersion;
    }

    public static boolean updateVersion(Versions versionToUpdate) {
        boolean isUpdated = false;
        System.out.println(versionToUpdate);
        String sqlQuery = "UPDATE versions SET first_artwork_id='" +
        		versionToUpdate.getFirstArtWorkId() + "', this_artwork_id='" +
        		versionToUpdate.getThisArtWorkId() + "' WHERE version_id=" +
        		versionToUpdate.getVersionId();
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating version type ");
        }
        return isUpdated;
    }

    public static boolean deleteVersionById(int versionId) {
        boolean isDeleted = false;
        String sqlQuery = "DELETE FROM versions WHERE version_id = " + versionId;
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDeleted = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork type ");
        }

        return isDeleted;
    }
}
