package ee.bcskoolitus.teoseregister.resources.utils;

import java.sql.Date;

public abstract class ResourcesUtils {
    public static java.sql.Date javaDateToSqlDate(Date dateToConvert){
        return new java.sql.Date(dateToConvert.getTime());
    }
}
