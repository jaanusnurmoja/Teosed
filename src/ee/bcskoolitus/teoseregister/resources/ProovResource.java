package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public abstract class ProovResource {

    public static List<Map<String, String>> getAllData(String myDbName) {
        List<Map<String, String>> sqlAnswers = new ArrayList<>();
        Map<String, String> tblStructsMy = new LinkedHashMap<>();
        tblStructsMy = getTableFieldsAndAlias(myDbName);
        String sqlQuery = "SELECT * FROM " + myDbName;
        try (ResultSet answerResults = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery).executeQuery();) {
            while (answerResults.next()) {
                Map<String, String> newRowInRS = new LinkedHashMap<>();
                for (String key : tblStructsMy.keySet()) {
                    newRowInRS.put(key, answerResults.getString
                            (tblStructsMy.get(key)));
                }
                sqlAnswers.add(newRowInRS);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting autorite andmed " + e.getMessage());
        }
        return sqlAnswers;

    }

    public static String addDataToTable(Map<String,
            String> newDataToAdd, String myDbName) {
        Map<String, String> fieldsAliases = getTableFieldsAndAlias(myDbName);
        Boolean second = false;
        int newKey = 0;
        String sqlFieldsInQuery = "(`";
        String sqlValuesInQuery = "('";
        for (String key : newDataToAdd.keySet()) {
            if (second == true) {
                sqlFieldsInQuery = sqlFieldsInQuery + ", `";
                sqlValuesInQuery = sqlValuesInQuery + ", '";
            }
            sqlFieldsInQuery = sqlFieldsInQuery + fieldsAliases.get(key)
                    + "`";
            sqlValuesInQuery = sqlValuesInQuery + newDataToAdd.get(key) + "'";
            second = true;
        }
        sqlFieldsInQuery = sqlFieldsInQuery + ")";
        sqlValuesInQuery = sqlValuesInQuery + ")";
        String sqlQuery = "INSERT INTO `" + myDbName + "` " + sqlFieldsInQuery
                + " VALUES " + sqlValuesInQuery + ";";
        try (Statement addDataStatement = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery);) {
            Integer addedResultCode = addDataStatement.executeUpdate
                    (sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (addedResultCode == 1) {
                ResultSet resultSet = addDataStatement.getGeneratedKeys();
                newKey = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new data to " + myDbName + ":" +
                    " " + e.getMessage());
        }

        return String.valueOf(newKey);
    }

    public static boolean deleteDataById(String myDbName, Map<String, String> dataToDelete) {
        boolean isDataDeleted = false;
        Map<String, String> fieldsAliases = getTableFieldsAndAlias(myDbName);
        int deleteId = 0;
        String deleteField = "";
        for (String key : dataToDelete.keySet()) {
            deleteId = Integer.parseInt(dataToDelete.get(key));
            deleteField = fieldsAliases.get(key);
        }
        String sqlQuery = "DELETE FROM " + myDbName + " WHERE `" + deleteField + "` = " +
                deleteId + ";";
        try (Statement deleteStatement = DatabaseConnection.getConnection().prepareStatement
                (sqlQuery);) {
            Integer resultCode = deleteStatement.executeUpdate(sqlQuery, Statement
                    .RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDataDeleted = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on deleteing data from " + myDbName);
        }
        return isDataDeleted;
    }

    public static boolean updateDataInTable(Map<String, String> dataToUpdate,
                                            String myTableName,
                                            String idColumnName) {
        boolean isDataUpdated = false;
        Map<String, String> fieldAliases = getTableFieldsAndAlias((myTableName));
        String sqlUpdates = "";
        String sqlWhere = "";
        boolean second = false;
        for (String key : dataToUpdate.keySet()) {
            if (key.equals(idColumnName)) {
                sqlWhere = sqlWhere + "`" + fieldAliases.get(key) + "` = " + dataToUpdate.get(key)
                        + ";";
            } else {
                if (second == true) {
                    sqlUpdates = sqlUpdates + ", ";
                }
                sqlUpdates = sqlUpdates + "`" + fieldAliases.get(key) + "` = '" + dataToUpdate
                        .get(key) + "'";
                second = true;
            }
        }
        String sqlQuery = "UPDATE " + myTableName + " SET " + sqlUpdates + " WHERE " + sqlWhere +
                ";";
        try (Statement dataUpdateStatement = DatabaseConnection.getConnection().prepareStatement
                (sqlQuery);) {
            Integer resultCode = dataUpdateStatement.executeUpdate(sqlQuery, Statement
                    .RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDataUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Eroor on deleteing data in table" + myTableName + ", " + e
                    .getMessage());
        }
        return isDataUpdated;
    }


    public static Map<String, String> getTableFieldsAndAlias(String myTblName) {
        Map<String, String> tblStructs = new LinkedHashMap<>();
        String dbSqlQuery = "SELECT * FROM w_structses WHERE table_name = '" +
                myTblName + "';";
        try (ResultSet dbAnswerResults = DatabaseConnection.getConnection()
                .prepareStatement(dbSqlQuery).executeQuery();) {
            while (dbAnswerResults.next()) {
                tblStructs.put(dbAnswerResults.getString("field_alias"),
                        dbAnswerResults.getString("field_name"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting tabeli struktuur" + e.getMessage());
        }
        return tblStructs;
    }


}
