package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.teoseregister.dao.Authors;
import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public abstract class AuthorsResources {

    public static List<Authors> getAllAuthor() {
        List<Authors> allAuthorList = new ArrayList<>();
        String sqlQuery = "SELECT * FROM author";
        try (ResultSet authorResults = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
                .executeQuery();) {
            while (authorResults.next()) {
                Authors authorData = new Authors();
                authorData.setAuthorId(authorResults.getInt("author_id"));
                authorData.setFirstName(authorResults.getString("first_name"));
                authorData.setLastName(authorResults.getString("last_name"));
                authorData.setAuthorName(authorResults.getString("first_name").concat(" ").concat(authorResults.getString("last_name")));
                allAuthorList.add(authorData);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting autorite andmed" + e.getMessage());
        }
        return allAuthorList;
    }

    public static Authors getAuthorById(int authorId) {
        Authors authorFromDb = new Authors();
        String sqlQuery = "SELECT * FROM author WHERE author_id = " +
                authorId;
        try (ResultSet results = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery).executeQuery();) {
            while (results.next()) {
                authorFromDb.setAuthorId(results.getInt("author_id"));
                authorFromDb.setFirstName(results.getString("first_name"));
                authorFromDb.setLastName(results.getString("last_name"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting specified author " + e.getMessage());
        }
        return authorFromDb;
    }

    public static Authors addNewAuthor(Authors newAuthorData) {
        String sqlQuery = "INSERT INTO author (first_name, last_name) VALUES " +
                "('" + newAuthorData.getFirstName() + "', '" + newAuthorData
                .getLastName() + "');";
        System.out.println(sqlQuery);
        try (Statement authorStatement = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery)) {
            Integer authorResultCode = authorStatement.executeUpdate
                    (sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (authorResultCode == 1) {
                ResultSet authorresultSet = authorStatement.getGeneratedKeys();
                while (authorresultSet.next()) {
                    newAuthorData.setAuthorId(authorresultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error on adding autor " + e.getMessage());
        }
        return newAuthorData;
    }

    public static boolean updateAuthorData(Authors authorToUpdate) {
        boolean isUpdated = false;
        String sqlQuery = "UPDATE author SET first_name='" + authorToUpdate
                .getFirstName()
                + "', " +
                "last_name='" + authorToUpdate.getLastName() + "' WHERE " +
                "author_id=" + authorToUpdate.getAuthorId() +
                ";";
        ;
        try (Statement statements = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery,
                    Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating autor" + e.getMessage());
        }
        return isUpdated;
    }

    public static boolean deleteAuthorData(int authorId) {
        boolean isDeleted = false;
        String sqlString = "DELETE FROM author WHERE author_id = " + authorId;
        try (Statement statements = DatabaseConnection.getConnection()
                .prepareStatement(sqlString);){
            Integer resultCode = statements.executeUpdate(sqlString,
                    Statement.RETURN_GENERATED_KEYS);
            if(resultCode == 1){
                isDeleted=true;
            }
        }catch (SQLException e) {
            System.out.println("Error on deleting autor andmed " + e.getMessage());
        }
        return isDeleted;
    }


}





