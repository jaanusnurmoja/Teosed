package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.teoseregister.dao.ArtworkTypes;
import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public abstract class ArtWorkTypeResource {

    public static List<ArtworkTypes> getAllArtworkTypes() {
        List<ArtworkTypes> allArtworkTypes = new ArrayList<>();
        String sqlQuery = "SELECT * FROM artwork_types";
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
                .executeQuery();) {
            while (results.next()) {
                ArtworkTypes artWorkType = new ArtworkTypes();
                artWorkType.setTypeId(results.getInt("type_id"));
                artWorkType.setTypeName(results.getString("type_name"));
                artWorkType.setTypeDescription(results.getString("type_description"));
                allArtworkTypes.add(artWorkType);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting alla artwork types " + e.getMessage());
        }
        return allArtworkTypes;
    }

    public static ArtworkTypes addArtWorkType(ArtworkTypes newArtWorkType) {
        String sqlQuery = "INSERT INTO `artwork_types`" +
                " (`type_name`, `type_description`) " + "VALUES ('" +
                newArtWorkType.getTypeName() + "', '" +
                newArtWorkType.getTypeDescription() + "');";
        try (Statement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                ResultSet resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    newArtWorkType.setTypeId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new artwork type");
        }
        return newArtWorkType;
    }

    public static ArtworkTypes getArtWorkTypeById(int artWorkId) {
        ArtworkTypes artWorkType = new ArtworkTypes();
        String sqlQuery = "SELECT * FROM artwork_types WHERE type_id = " +
                artWorkId;
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery).executeQuery();) {
            while (results.next()) {
                artWorkType.setTypeId(results.getInt("type_id"));
                artWorkType.setTypeName(results.getString("type_name"));
                artWorkType.setTypeDescription(results.getString("type_description"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting course types " + e.getMessage());
        }
        return artWorkType;
    }

    public static boolean updateArtWorkType(ArtworkTypes artWorkToUpdate) {
        boolean isUpdated = false;
        String sqlQuery = "UPDATE artwork_types SET type_name='" +
                artWorkToUpdate.getTypeName() + "', type_description='" +
                artWorkToUpdate.getTypeDescription() + "' WHERE type_id=" +
                artWorkToUpdate.getTypeId();
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork type ");
        }
        return isUpdated;
    }

    public static boolean deleteArtWorkTypeById(int artWorkTypeId) {
        boolean isDeleted = false;
        String sqlQuery = "DELETE FROM artwork_types WHERE type_id = " + artWorkTypeId;
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDeleted = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork type ");
        }


        return isDeleted;
    }


}
