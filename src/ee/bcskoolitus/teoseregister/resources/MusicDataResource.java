package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.teoseregister.dao.MusicData;
import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public abstract class MusicDataResource {

    public static List<MusicData> getAllMusicData() {
        List<MusicData> allMusicData = new ArrayList<>();
        String sqlQuery = "SELECT * FROM music_data";
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
                .executeQuery();) {
            while (results.next()) {
                MusicData musicData = new MusicData();
                musicData.setMusicId(results.getInt("music_id"));
                musicData.setArtWorkId(results.getInt("artwork_id"));
                musicData.setLength(results.getTime("length"));
                musicData.setIsInstrumental(results.getBoolean("is_instrumental"));
                allMusicData.add(musicData);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting alla artwork types " + e.getMessage());
        }
        return allMusicData;
    }

    public static MusicData addMusicData(MusicData newMusicData) {
        String sqlQuery = "INSERT INTO `music_data`" +
                " (`artwork_id`, `length`, `is_instrumental`) " + "VALUES ('" +
                newMusicData.getArtWorkId() + "', '" +
                newMusicData.getLength() + "', '" + 
                newMusicData.getIsInstrumental() + "');";
        try (Statement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                ResultSet resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    newMusicData.setMusicId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new artwork type");
        }
        return newMusicData;
    }

    public static MusicData getMusicDataById(int musicId) {
        MusicData musicData = new MusicData();
        String sqlQuery = "SELECT * FROM music_data WHERE music_id = " +
                musicId;
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery).executeQuery();) {
            while (results.next()) {
                musicData.setMusicId(results.getInt("music_id"));
                musicData.setArtWorkId(results.getInt("artwork_id"));
                musicData.setLength(results.getTime("length"));
                musicData.setIsInstrumental(results.getBoolean("is_instrumental"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting course types " + e.getMessage());
        }
        return musicData;
    }

    public static boolean updateMusicData(MusicData musicDataToUpdate) {
        boolean isUpdated = false;
        String sqlQuery = "UPDATE music_data SET artwork_id='" +
                musicDataToUpdate.getArtWorkId() + "', length='" +
                musicDataToUpdate.getLength() + "' WHERE music_id=" +
                musicDataToUpdate.getMusicId();
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork type ");
        }
        return isUpdated;
    }

    public static boolean deleteMusicDataById(int musicDataId) {
        boolean isDeleted = false;
        String sqlQuery = "DELETE FROM music_data WHERE music_id = " + musicDataId;
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDeleted = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork type ");
        }


        return isDeleted;
    }


}
