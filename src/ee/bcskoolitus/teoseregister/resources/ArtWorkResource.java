package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.teoseregister.dao.ArtWorkUnits;
import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public abstract class ArtWorkResource {

    public static List<ArtWorkUnits> getAllArtWorks() {
        List<ArtWorkUnits> allArtWorks = new ArrayList<>();
        String sqlQuery = "SELECT * FROM artwork_units";
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
                .executeQuery();) {
            while (results.next()) {
                ArtWorkUnits ArtWork = new ArtWorkUnits();
                ArtWork.setArtWorkId(results.getInt("artwork_id"));
                ArtWork.setArtWorkName(results.getString("artwork_name"));
                ArtWork.setArtWorkCreationTime(results.getDate("artwork_creation_time"));
                ArtWork.setFirstPresentation(results.getDate("first_presentation"));
                ArtWork.setPlaceOfFirstPresentation(results.getString("place_of_first_presentation"));
                ArtWork.setTypeId(results.getInt("Type_ID"));
                allArtWorks.add(ArtWork);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting all artworks " + e.getMessage());
        }
        return allArtWorks;
    }


   public static List<ArtWorkUnits> getArtWorksByTypeId(int typeId) {
        List<ArtWorkUnits> artWorksByType = getAllArtWorks();
        
        String sqlQuery = "SELECT * FROM artwork_units WHERE Type_ID = " + typeId;
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
                .executeQuery();) {
            while (results.next()) {
                ArtWorkUnits ArtWork = new ArtWorkUnits();
                ArtWork.setArtWorkId(results.getInt("artwork_id"));
                ArtWork.setArtWorkName(results.getString("artwork_name"));
                ArtWork.setArtWorkCreationTime(results.getDate("artwork_creation_time"));
                ArtWork.setFirstPresentation(results.getDate("first_presentation"));
                ArtWork.setPlaceOfFirstPresentation(results.getString("place_of_first_presentation"));
                ArtWork.setTypeId(results.getInt("Type_ID"));
                artWorksByType.add(ArtWork);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting all artworks " + e.getMessage());
        }
        return artWorksByType;
    }

    public static ArtWorkUnits getArtWorkById(int artWorkId) {
        ArtWorkUnits ArtWork = new ArtWorkUnits();
        
        String sqlQuery = "SELECT u.*, GROUP_CONCAT(\n" + 
        		"	JSON_OBJECT\n" + 
        		"	( \n" + 
        		"		r.role_name, \n" + 
        		"		CONCAT_WS(' ', p.first_name, p.last_name), \n" + 
        		"		'artWorkAuthorsID', a.artwork_authors_id, \n" + 
        		"		'artWorkId', a.artwork_id, \n" + 
        		"		'roleId', a.role_id, \n" + 
        		"		'roleName', r.role_name, \n" + 
        		"		'authorId', a.author_id)\n" + 
        		"	) \n" + 
        		"		as author_data \n" + 
        		"		FROM artwork_units u, artwork_usage usg, artwork_authors a, roles r, author p \n" + 
        		"		WHERE p.author_id = a.author_id \n" + 
        		"		AND r.role_id = a.role_id \n" + 
        		"		AND a.artwork_id IN (usg.parent_artwork, usg.child_artwork) \n" + 
        		"		AND usg.parent_artwork = u.artwork_id \n" + 
        		"		AND u.artwork_id = " + artWorkId;
        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery).executeQuery();) {
            while (results.next()) {
                ArtWork.setArtWorkId(results.getInt("artwork_id"));
                ArtWork.setArtWorkName(results.getString("artwork_name"));
                ArtWork.setArtWorkCreationTime(results.getDate("artwork_creation_time"));
                ArtWork.setFirstPresentation(results.getDate("first_presentation"));
                ArtWork.setPlaceOfFirstPresentation(results.getString("place_of_first_presentation"));
                ArtWork.setTypeId(results.getInt("Type_ID"));
                ArtWork.setAuthorData(results.getString("author_data"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting artwork details " + e.getMessage());
        }
        return ArtWork;
    }

    public static ArtWorkUnits addArtWork(ArtWorkUnits newArtWork) {
        String sqlQuery = "INSERT INTO `artwork_units`" +
                " (`artwork_name`, `artwork_creation_time`, `first_presentation`, `place_of_first_presentation`, `Type_ID`) " + "VALUES ('" +
                newArtWork.getArtWorkName() + "', '" +
                newArtWork.getArtWorkCreationTime() + "', '" +
                newArtWork.getFirstPresentation() + "', '" +
                newArtWork.getPlaceOfFirstPresentation() + "', '" +
                newArtWork.getTypeId() + "');";
        try (Statement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                ResultSet resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    newArtWork.setArtWorkId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new artwork ");
        }
        return newArtWork;
    }

    public static boolean updateArtWork(ArtWorkUnits artWorkToUpdate) {
        boolean isUpdated = false;
        String sqlQuery = "UPDATE artwork_units SET "
        		+ "artwork_name='" + artWorkToUpdate.getArtWorkName() + "', "
        		+ "artwork_creation_time='" + artWorkToUpdate.getArtWorkCreationTime() + "', "
        		+ "first_presentation='" + artWorkToUpdate.getFirstPresentation() + "', "
        		+ "place_of_first_presentation='" + artWorkToUpdate.getPlaceOfFirstPresentation() + "', "
        		+ "Type_ID='" + artWorkToUpdate.getTypeId() + "' "
        		+ "WHERE artwork_id=" + artWorkToUpdate.getArtWorkId();
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork ");
        }
        return isUpdated;
    }

    public static boolean deleteArtWorkById(int artWorkId) {
        boolean isDeleted = false;
        String sqlQuery = "DELETE FROM artwork_units WHERE artwork_id = " + artWorkId;
        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDeleted = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on updating artwork");
        }


        return isDeleted;
    }


}
