package ee.bcskoolitus.teoseregister.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.teoseregister.dao.ArtWorkHasAuthors;
import ee.bcskoolitus.teoseregister.resources.utils.DatabaseConnection;

public class ArtWorkHasAuthorsResource extends ArtWorkResource
{
	   public static List<ArtWorkHasAuthors> getAllArtWorkHasAuthors() {
	        List<ArtWorkHasAuthors> allArtWorkHasAuthors = new ArrayList<>();
	        String sqlQuery = "SELECT * FROM artwork_authors";
	        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery)
	                .executeQuery();) {
	            while (results.next()) {
	                ArtWorkHasAuthors artWorkHasAuthors = new ArtWorkHasAuthors();
	                artWorkHasAuthors.setArtWorkAuthorsId(results.getInt("artwork_authors_id"));
	                artWorkHasAuthors.setArtWorkId(results.getInt("artwork_id"));
	                artWorkHasAuthors.setAuthorId(results.getInt("author_id"));
	                artWorkHasAuthors.setRoleId(results.getInt("role_id"));
	                allArtWorkHasAuthors.add(artWorkHasAuthors);
	            }
	        } catch (SQLException e) {
	            System.out.println("Error on getting alla artwork types " + e.getMessage());
	        }
	        return allArtWorkHasAuthors;
	    }

	    public static ArtWorkHasAuthors addArtWorkHasAuthors(ArtWorkHasAuthors newArtWorkHasAuthors) {
	        String sqlQuery = "INSERT INTO `artwork_authors`" +
	                " (`artwork_authors_id`, `artwork_id`, `author_id`, `role_id`) " + "VALUES ('" +
	                newArtWorkHasAuthors.getArtWorkId() + "', '" +
	                newArtWorkHasAuthors.getAuthorId() + "', '" + 
	                newArtWorkHasAuthors.getRoleId() + "');";
	        try (Statement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
	            Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	            if (resultCode == 1) {
	                ResultSet resultSet = statement.getGeneratedKeys();
	                while (resultSet.next()) {
	                    newArtWorkHasAuthors.setArtWorkAuthorsId(resultSet.getInt(1));
	                }
	            }
	        } catch (SQLException e) {
	            System.out.println("Error on adding new artwork authors");
	        }
	        return newArtWorkHasAuthors;
	    }

	    public static ArtWorkHasAuthors getArtWorkHasAuthorsById(int artWorkAuthorsId) {
	        ArtWorkHasAuthors artWorkAuthors = new ArtWorkHasAuthors();
	        String sqlQuery = "SELECT * FROM artwork_authors WHERE artwork_authors_id = " + artWorkAuthorsId;
	        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery).executeQuery();) 
	        {
	            while (results.next()) {
	                artWorkAuthors.setArtWorkAuthorsId(results.getInt("artwork_authors_id"));
	                artWorkAuthors.setArtWorkId(results.getInt("artwork_id"));
	                artWorkAuthors.setAuthorId(results.getInt("author_id"));
	                artWorkAuthors.setRoleId(results.getInt("role_id"));
	            }
	        } catch (SQLException e) {
	            System.out.println("Error on getting artwork authors " + e.getMessage());
	        }
	        return artWorkAuthors;
	    }
	    
	    /*Jaanus: pealtnäha omamoodi "trikiga" päring, kuid just sellisel kujul viib ta kokku 
	     * teose ja autorid juhul, kui teos kasutab ka teist teost (nt muusika ja tekst)
	     * ning me ei taha andmeid dubleerida, st muusika autor jääb üksnes 
	     * muusikakirje juurde ning teksti autor oma teksti andmete juurde.
	    */

	    public static List<ArtWorkHasAuthors> getArtWorkHasAuthorsByArtWorkId(int thisArtWorkId) {
	    	List<ArtWorkHasAuthors> thisArtWorkHasAuthors = new ArrayList<>();
	        String sqlQuery ="SELECT this_artwork_id, artwork_authors_id, artwork_id, a.role_id, r.role_name, ar.author_id, CONCAT_WS(' ', first_name, last_name) AS author_name FROM (SELECT v1.first_artwork_id, v1.this_artwork_id, u.child_artwork,     v2.first_artwork_id first2 FROM  versions v1 LEFT JOIN artwork_usage u ON u.parent_artwork = v1.this_artwork_id LEFT JOIN versions v2 ON v2.this_artwork_id = u.child_artwork WHERE v1.this_artwork_id = " + thisArtWorkId +  ") AS workdata LEFT JOIN artwork_authors a ON a.artwork_id = workdata.first_artwork_id OR a.artwork_id = workdata.this_artwork_id OR a.artwork_id = workdata.child_artwork OR a.artwork_id = workdata.first2 JOIN roles r ON r.role_id = a.role_id JOIN author ar ON ar.author_id = a.author_id";
	        
	        
	        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery).executeQuery();) {
	            while (results.next()) {
	            	ArtWorkHasAuthors ArtWorkHasAuthors = new ArtWorkHasAuthors();
	            	ArtWorkHasAuthors.setThisArtWorkId(results.getInt("this_artwork_id"));
	            	ArtWorkHasAuthors.setArtWorkAuthorsId(results.getInt("artwork_authors_id"));
	            	ArtWorkHasAuthors.setArtWorkId(results.getInt("artwork_id"));
	            	ArtWorkHasAuthors.setRoleId(results.getInt("role_id"));
	            	ArtWorkHasAuthors.setRoleName(results.getString("role_name"));
	            	ArtWorkHasAuthors.setAuthorId(results.getInt("author_id"));
	            	ArtWorkHasAuthors.setAuthorName(results.getString("author_name"));
	            	thisArtWorkHasAuthors.add(ArtWorkHasAuthors);
	            }
	        } catch (SQLException e) {
	            System.out.println("Error on getting artwork authors " + e.getMessage());
	        }
	        return thisArtWorkHasAuthors;
	    }

/*
	    public static ArtWorkHasAuthors getArtWorkHasAuthorsByArtWorkId(int artWorkId) {
	        ArtWorkHasAuthors thisArtWorkHasAuthors = new ArtWorkHasAuthors();
	        String sqlQuery = "SELECT * FROM artwork_authors WHERE artwork_id = " +
	        		artWorkId;
	        try (ResultSet results = DatabaseConnection.getConnection().prepareStatement(sqlQuery).executeQuery();) {
	            while (results.next()) {
	            	thisArtWorkHasAuthors.setArtWorkAuthorsId(results.getInt("artwork_authors_id"));
	            	thisArtWorkHasAuthors.setArtWorkId(results.getInt("artwork_id"));
	            	thisArtWorkHasAuthors.setAuthorId(results.getInt("author_id"));
	            	thisArtWorkHasAuthors.setRoleId(results.getInt("role_id"));
	            }
	        } catch (SQLException e) {
	            System.out.println("Error on getting artwork authors " + e.getMessage());
	        }
	        return thisArtWorkHasAuthors;
	    }
*/
	    public static boolean updateArtWorkHasAuthors(ArtWorkHasAuthors artWorkHasAuthorsToUpdate) {
	        boolean isUpdated = false;
	        String sqlQuery = "UPDATE artwork_authors SET artwork_id='" +
	                artWorkHasAuthorsToUpdate.getArtWorkId() + "', author_id='" +
	                artWorkHasAuthorsToUpdate.getAuthorId() + "', role_id='" +
	                artWorkHasAuthorsToUpdate.getRoleId() + "' WHERE artwork_authors_id=" +
	                artWorkHasAuthorsToUpdate.getArtWorkAuthorsId();
	        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
	            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	            if (resultCode == 1) {
	                isUpdated = true;
	            }
	        } catch (SQLException e) {
	            System.out.println("Error on updating artwork authors ");
	        }
	        return isUpdated;
	    }

	    public static boolean deleteArtWorkHasAuthorsById(int artWorkHasAuthorsId) {
	        boolean isDeleted = false;
	        String sqlQuery = "DELETE FROM artwork_authors WHERE artwork_authors_id = " + artWorkHasAuthorsId;
	        try (Statement statements = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
	            Integer resultCode = statements.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	            if (resultCode == 1) {
	                isDeleted = true;
	            }
	        } catch (SQLException e) {
	            System.out.println("Error on updating artwork authors ");
	        }


	        return isDeleted;
	    }
	
}
