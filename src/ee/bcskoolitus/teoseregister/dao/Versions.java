package ee.bcskoolitus.teoseregister.dao;

public class Versions
{
	private int versionId;
	private int firstArtWorkId;
	private int thisArtWorkId;
	
	public int getVersionId()
	{
		return versionId;
	}
	public void setVersionId(int versionId)
	{
		this.versionId = versionId;
	}
	public int getFirstArtWorkId()
	{
		return firstArtWorkId;
	}
	public void setFirstArtWorkId(int firstArtWorkId)
	{
		this.firstArtWorkId = firstArtWorkId;
	}
	public int getThisArtWorkId()
	{
		return thisArtWorkId;
	}
	public void setThisArtWorkId(int thisArtWorkId)
	{
		this.thisArtWorkId = thisArtWorkId;
	}
	
	@Override
	public String toString()
	{
		return "versions:[versionId = " + versionId + " firstArtWorkId = " + firstArtWorkId + "thisArtWorkId = " + thisArtWorkId + "]";
	}
	
}
