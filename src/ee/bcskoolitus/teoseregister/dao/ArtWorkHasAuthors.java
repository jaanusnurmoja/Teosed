package ee.bcskoolitus.teoseregister.dao;

public class ArtWorkHasAuthors
{
	private int artWorkAuthorsId;
	private int artWorkId;
	private int firstArtWorkId;
	private int thisArtWorkId;
	private int roleId;
	private String roleName;
	private int authorId;
	private String authorName;
	private int parentArtWorkId;
	private int childArtWorkId;
	
	public int getArtWorkAuthorsId()
	{
		return artWorkAuthorsId;
	}
	public void setArtWorkAuthorsId(int artWorkAuthorsId)
	{
		this.artWorkAuthorsId = artWorkAuthorsId;
	}
	public int getArtWorkId()
	{
		return artWorkId;
	}
	public void setArtWorkId(int artWorkId)
	{
		this.artWorkId = artWorkId;
	}
	public int getFirstArtWorkId()
	{
		return firstArtWorkId;
	}
	public void setFirstArtWorkId(int firstArtWorkId)
	{
		this.firstArtWorkId = firstArtWorkId;
	}
	public int getThisArtWorkId()
	{
		return thisArtWorkId;
	}
	public void setThisArtWorkId(int thisArtWorkId)
	{
		this.thisArtWorkId = thisArtWorkId;
	}
	public int getRoleId()
	{
		return roleId;
	}
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}
	
	public String getRoleName()
	{	
		return roleName;
	}
	
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}
	
	public int getAuthorId()
	{
		return authorId;
	}
	public void setAuthorId(int authorId)
	{
		this.authorId = authorId;
	}
	
	public String getAuthorName()
	{
		return authorName;
	}
	
	public void setAuthorName(String authorName)
	{
		this.authorName = authorName;
	}
	
	public int getParentArtWorkId()
	{
		return parentArtWorkId;
	}
	public void setParentArtWorkId(int parentArtWorkId)
	{
		this.parentArtWorkId = parentArtWorkId;
	}
	public int getChildArtWorkId()
	{
		return childArtWorkId;
	}
	public void setChildArtWorkId(int childArtWorkId)
	{
		this.childArtWorkId = childArtWorkId;
	}


	@Override
    public String toString() 
	{
        return "artWorkHasAuthors[" + "\n" +
                "firstArtWorkId=" + firstArtWorkId + "\n" +
                "thisArtWorkId=" + thisArtWorkId + "\n" +
               "parentArtWorkId=" + parentArtWorkId + "\n" +
                "childArtWorkId=" + childArtWorkId + "\n" +
                "artWorkAuthorsId=" + artWorkAuthorsId + "\n" +
                ", artWorkId=" + artWorkId + "\n" +
                ", roleId='" + roleId + "\n" +
                ", roleName='" + roleName + "\n" +
               ", authorId='" + authorId + "\n" +
               ", authorName='" + authorName + "\n" +
               ']';
    }
}
