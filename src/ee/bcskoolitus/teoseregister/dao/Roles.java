package ee.bcskoolitus.teoseregister.dao;

public class Roles
{
	private int roleId;
	private String roleName;
	private String eayCode;
	
	public int getRoleId()
	{
		return roleId;
	}
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}
	public String getRoleName()
	{
		return roleName;
	}
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}
	public String getEayCode()
	{
		return eayCode;
	}
	public void setEayCode(String eayCode)
	{
		this.eayCode = eayCode;
	}
	
	@Override
	public String toString()
	{
		return "roles:[roleId: " + roleId + "\n"
				+ "roleName: " + roleName + "\n"
				+ "eayCode: " + eayCode + "]";
	}
	
}
