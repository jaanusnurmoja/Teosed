package ee.bcskoolitus.teoseregister.dao;

import java.sql.Date;

public class ArtWorkUnits
{
	int artWorkId;
	String artWorkName;
	Date artWorkCreationTime;
	Date firstPresentation;
	String placeOfFirstPresentation;
	int typeId;
	String authorData; 

	public int getArtWorkId()
	{
		return artWorkId;
	}

	public void setArtWorkId(int artWorkId)
	{
		this.artWorkId = artWorkId;
	}

	public String getArtWorkName()
	{
		return artWorkName;
	}

	public void setArtWorkName(String artWorkName)
	{
		this.artWorkName = artWorkName;
	}

	public Date getArtWorkCreationTime()
	{
		return artWorkCreationTime;
	}

	public void setArtWorkCreationTime(Date artWorkCreationTime)
	{
		this.artWorkCreationTime = artWorkCreationTime;
	}

	public Date getFirstPresentation()
	{
		return firstPresentation;
	}

	public void setFirstPresentation(Date firstPresentation)
	{
		this.firstPresentation = firstPresentation;
	}

	public String getPlaceOfFirstPresentation()
	{
		return placeOfFirstPresentation;
	}

	public void setPlaceOfFirstPresentation(String placeOfFirstPresentation)
	{
		this.placeOfFirstPresentation = placeOfFirstPresentation;
	}

	public int getTypeId()
	{
		return typeId;
	}

	public void setTypeId(int typeId)
	{
		this.typeId = typeId;
	}
	
	public String getAuthorData()
	{
		return authorData;
	}

	public void setAuthorData(String authorData)
	{
		this.authorData = authorData;
	}

	@Override
	public String toString()
	{
		String s = "Teosed: "
				+ "["
				+ "artWorkId = " + artWorkId + "\n"
				+ "artWorkName = " + artWorkName + "\n"
				+ " artWorkCreationTime = " + artWorkCreationTime + "\n"
				+ " firstPresentation = " + firstPresentation + "\n"
				+ " placeOfFirstPresentation = " + placeOfFirstPresentation + "\n"
				+ " typeId = " + typeId + "\n" 
				+ " authorData = " + authorData +  "\n" 
				+ "]"; 
		return s;
	}


}
