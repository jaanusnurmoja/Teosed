package ee.bcskoolitus.teoseregister.dao;

import java.sql.Time;

public class MusicData
{
	private int musicId;
	private int artWorkId;
	private Time length;
	private Boolean isInstrumental;
	public int getMusicId()
	{
		return musicId;
	}
	public void setMusicId(int musicId)
	{
		this.musicId = musicId;
	}
	public int getArtWorkId()
	{
		return artWorkId;
	}
	public void setArtWorkId(int artWorkId)
	{
		this.artWorkId = artWorkId;
	}
	public Time getLength()
	{
		return length;
	}
	public void setLength(Time length)
	{
		this.length = length;
	}
	public Boolean getIsInstrumental()
	{
		return isInstrumental;
	}
	public void setIsInstrumental(Boolean isInstrumental)
	{
		this.isInstrumental = isInstrumental;
	}
	
	@Override
	public String toString()
	{
		return "musicData:[musicId = " + musicId + " artWorkId = " + artWorkId + " length = " + length + " isInstrumental = " + isInstrumental + "]";
	}

}
