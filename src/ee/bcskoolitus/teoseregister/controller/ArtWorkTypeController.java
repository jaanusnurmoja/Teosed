package ee.bcskoolitus.teoseregister.controller;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.teoseregister.dao.ArtworkTypes;
import ee.bcskoolitus.teoseregister.resources.ArtWorkTypeResource;

@Path("/artworktypes")
public class ArtWorkTypeController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ArtworkTypes> getAllArtWorkType() {
        return ArtWorkTypeResource.getAllArtworkTypes();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArtworkTypes addNewArtWorkType(ArtworkTypes newArtWorkType) {
        System.out.println(newArtWorkType);
        return ArtWorkTypeResource.addArtWorkType(newArtWorkType);
    }

    @DELETE
    @Path("/{artWorkTypeId}")
    public void deleteArtWorkTypeById(@PathParam("artWorkTypeId") int artWorkTypeNumber) {
        ArtWorkTypeResource.deleteArtWorkTypeById(artWorkTypeNumber);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public boolean updateArtWorkType(ArtworkTypes artWorkType){
        return ArtWorkTypeResource.updateArtWorkType(artWorkType);
    }
}
