package ee.bcskoolitus.teoseregister.controller;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.teoseregister.dao.ArtWorkHasAuthors;
import ee.bcskoolitus.teoseregister.resources.ArtWorkHasAuthorsResource;

@Path("/workauthors")
public class ArtWorkHasAuthorsController
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ArtWorkHasAuthors> getAllArtWorkHasAuthors() {
        return ArtWorkHasAuthorsResource.getAllArtWorkHasAuthors();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{artWorkAuthorsId}")
    public void getArtWorkHasAuthorsById(@PathParam("artWorkAuthorsId") int artWorkAuthorsId) {
        ArtWorkHasAuthorsResource.getArtWorkHasAuthorsById(artWorkAuthorsId);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/work/{thisArtWorkId}")
    public List<ArtWorkHasAuthors> getArtWorkHasAuthorsByArtWorkId(@PathParam("thisArtWorkId") int thisArtWorkId) {
        return ArtWorkHasAuthorsResource.getArtWorkHasAuthorsByArtWorkId(thisArtWorkId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArtWorkHasAuthors addNewArtWorkHasAuthors(ArtWorkHasAuthors newArtWorkHasAuthors) {
        System.out.println(newArtWorkHasAuthors);
        return ArtWorkHasAuthorsResource.addArtWorkHasAuthors(newArtWorkHasAuthors);
    }

    @DELETE
    @Path("/{artWorkAuthorsId}")
    public void deleteArtWorkHasAuthorsById(@PathParam("artWorkAuthorsId") int artWorkHasAuthorsId) {
        ArtWorkHasAuthorsResource.deleteArtWorkHasAuthorsById(artWorkHasAuthorsId);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public boolean updateArtWorkHasAuthors(ArtWorkHasAuthors artWorkHasAuthors){
        return ArtWorkHasAuthorsResource.updateArtWorkHasAuthors(artWorkHasAuthors);
    }
}
