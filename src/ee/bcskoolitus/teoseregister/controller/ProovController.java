package ee.bcskoolitus.teoseregister.controller;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ee.bcskoolitus.teoseregister.resources.ProovResource;


@Path("/proov")

public class ProovController {


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{tblName}")
    public static String getData(@PathParam("tblName") String tableName) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(ProovResource.getAllData(tableName));
        return jsonString;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{tblName}")
    public static String addNewDataToDb(@PathParam("tblName") String
                                                tableName, String dataAddMap) {
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<LinkedHashMap<String, String>>() {
        }
                .getType();
        Map<String, String> receivadDataMap = gson.fromJson(String.valueOf(dataAddMap),
                stringStringMap);
        return ProovResource.addDataToTable(receivadDataMap, tableName);

    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{tblName}")
    public static boolean deleteDataFromDb(@PathParam("tblName") String tableName, String
            dataDeleteMap) {
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<LinkedHashMap<String, String>>() {
        }.getType();
        Map<String, String> receivadDataMap = gson.fromJson(String.valueOf(dataDeleteMap),
                stringStringMap);
        return ProovResource.deleteDataById(tableName, receivadDataMap);

    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{tblName}/{idAlias}")
    public boolean updateDataInTable(
            @PathParam("tblName") String tableName,
            @PathParam("idAlias") String idName,
            String updateDataMap) {
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<LinkedHashMap<String, String>>() {
        }.getType();
        Map<String, String> receivadDataMap = gson.fromJson(String.valueOf(updateDataMap),
                stringStringMap);
        return ProovResource.updateDataInTable(receivadDataMap, tableName, idName);
    }


}
