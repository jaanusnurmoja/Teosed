package ee.bcskoolitus.teoseregister.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.teoseregister.dao.Authors;
import ee.bcskoolitus.teoseregister.resources.AuthorsResources;


@Path("/author")

public class AuthorDataController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Authors> getAllAuthors(){
        return AuthorsResources.getAllAuthor();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{authorId}")
    public Authors getAutorById(@PathParam("authorId") int authorId){
        return AuthorsResources.getAuthorById(authorId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Authors addNewAuthor(Authors newAuthorData){
        return AuthorsResources.addNewAuthor(newAuthorData);
    }

    @DELETE
    @Path("/{authorIdNr}")
    public void deleteAuthorById(@PathParam("authorIdNr") int authorIdNumber) {
        AuthorsResources.deleteAuthorData(authorIdNumber);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public boolean updateAuthorData(Authors authorToChange){
        return AuthorsResources.updateAuthorData(authorToChange);
    }


}
