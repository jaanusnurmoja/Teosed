package ee.bcskoolitus.teoseregister.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.teoseregister.dao.Roles;
import ee.bcskoolitus.teoseregister.resources.RolesResource;



@Path ("/roles")

public class RolesController
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Roles> getAllRoles()
    {
        List<Roles> tempList = new ArrayList<>();
        tempList = RolesResource.getAllRoles();
        
    	return  tempList;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Roles addNewRole(Roles newRole)
    {
        return RolesResource.addNewRole(newRole);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/update")
    public boolean updateRole(Roles modifyRole){
        System.out.println("Saabusin PUT controllerisse");
    	return RolesResource.updateRole(modifyRole);
    }
    
    @DELETE
    @Path("/delete/{RoleId}")
    public void deleteRoleById(@PathParam("RoleId") int roleId) {
    	RolesResource.deleteRoleById(roleId);
    }
    
}
