package ee.bcskoolitus.teoseregister.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.teoseregister.dao.Versions;
import ee.bcskoolitus.teoseregister.resources.VersionsResource;

@Path ("/versions")
public class VersionsController
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Versions> getAllVersions()
    {
        List<Versions> tempList = new ArrayList<>();
        tempList = VersionsResource.getAllVersions();
        
    	return  tempList;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Versions addNewVersion(Versions newVersion)
    {
        return VersionsResource.addNewVersion(newVersion);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/update")
    public boolean updateVersion(Versions modifyVersion){
        System.out.println("Saabusin PUT controllerisse");
    	return VersionsResource.updateVersion(modifyVersion);
    }
    
    @DELETE
    @Path("/delete/{VersionId}")
    public void deleteVersionById(@PathParam("VersionId") int versionId) {
    	VersionsResource.deleteVersionById(versionId);
    }
    
}
