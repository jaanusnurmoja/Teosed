package ee.bcskoolitus.teoseregister.controller;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.teoseregister.dao.MusicData;
import ee.bcskoolitus.teoseregister.resources.MusicDataResource;

@Path("/musicdata")
public class MusicDataController
{

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MusicData> getAllMusicData() {
        return MusicDataResource.getAllMusicData();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MusicData addNewMusicData(MusicData newMusicData) {
        System.out.println(newMusicData);
        return MusicDataResource.addMusicData(newMusicData);
    }

    @DELETE
    @Path("/{musicId}")
    public void deleteMusicDataById(@PathParam("musicId") int musicDataNumber) {
        MusicDataResource.deleteMusicDataById(musicDataNumber);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public boolean updateMusicData(MusicData musicData){
        return MusicDataResource.updateMusicData(musicData);
    }
}
