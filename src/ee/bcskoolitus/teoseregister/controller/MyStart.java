package ee.bcskoolitus.teoseregister.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/startIt")
public class MyStart {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String startIt(){
        return "Started";
    }
}
