package ee.bcskoolitus.teoseregister.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.teoseregister.dao.ArtWorkUnits;
import ee.bcskoolitus.teoseregister.resources.ArtWorkResource;

@Path("/works")

public class ArtWorkUnitsController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ArtWorkUnits> getAllArtWorks(){
        return ArtWorkResource.getAllArtWorks();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{artWorkId}")
    public ArtWorkUnits getArtWorkById(@PathParam("artWorkId") int artWorkId){
        return ArtWorkResource.getArtWorkById(artWorkId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArtWorkUnits addNewAuthor(ArtWorkUnits newArtWorkData){
        return ArtWorkResource.addArtWork(newArtWorkData);
    }

    @DELETE
    @Path("/delete/{artWorkId}")
    public void deleteArtWork(@PathParam("artWorkId") int artWorkId) {
    	ArtWorkResource.deleteArtWorkById(artWorkId);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/update")
    public boolean updateArtWork(ArtWorkUnits artWorkToChange){
        return ArtWorkResource.updateArtWork(artWorkToChange);
    }


}
