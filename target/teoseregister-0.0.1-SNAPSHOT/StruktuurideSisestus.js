var allFieldList;
var urlTextTabels = "http://localhost:8080/teoseregister/rest/proov/w_structses";
var conText = "application/json; charset=utf-8";

function getAllTableRows() {
    $.getJSON(urlTextTabels, function (allFieldsData) {
        allFieldList = allFieldsData;
        var fieldTableBody = "";
        for (var i = 0; i < allFieldsData.length; i++) {
            fieldTableBody = fieldTableBody +
                "<tr><td>" + allFieldsData[i].structID +
                "</td><td>" + allFieldsData[i].tableName +
                "</td><td>" + allFieldsData[i].fieldName +
                "</td><td>" + allFieldsData[i].fieldAlias +
                "</td><td><button type='button' onclick='deleteTableData(" +
                allFieldsData[i].structID + ")'>Delete</button>" +
                "<button type='button'" + " onclick='changeTableData(" +
                allFieldsData[i].structID + ")'>Modify</button>" +
                "</td></tr>"
        }
        document.getElementById("TabelesData").innerHTML = fieldTableBody;
    })
}

getAllTableRows();


function addTableInfo() {
    var jsonDataAdd = {
        "tableName": document.getElementById("newTableName").value,
        "fieldName": document.getElementById("newColumnName").value,
        "fieldAlias": document.getElementById("newColumnAlias").value
    };
    var jsonDataAddString = JSON.stringify(jsonDataAdd);
    $.ajax({
        url: urlTextTabels,
        type: "POST",
        data: jsonDataAddString,
        contentType: conText,
        success: function () {
            emptyStruktuurideSisestusDataLabel();
            getAllTableRows();
        }
    })
}

function deleteTableData(dataToDeleteId) {
    var jsonData = {
        "structID": dataToDeleteId
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlTextTabels,
        type: "DELETE",
        data: jsonDataString,
        contentType: conText,
        success: function () {
            emptyStruktuurideSisestusDataLabel();
            getAllTableRows();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Eroor on deleting data");
        }
    })
}

function changeTableData(dataToChangeId) {
    for (var i = 0; i < allFieldList.length; i++) {
        document.getElementById("modifyTableId").value = dataToChangeId;
        document.getElementById("modifyTableName").value = allFieldList[i].tableName;
        document.getElementById("modifyTableColumnName").value = allFieldList[i].fieldName;
        document.getElementById("modifyTableAliasName").value = allFieldList[i].fieldAlias;
    }
}

function saveTableDataChanges() {
    var jsonDataUpdate = {
        "structID": document.getElementById("modifyTableId").value,
        "tableName": document.getElementById("modifyTableName").value,
        "fieldName": document.getElementById("modifyTableColumnName").value,
        "fieldAlias": document.getElementById("modifyTableAliasName").value
    };
    jsonDataStringForUpdate = JSON.stringify(jsonDataUpdate);
    var urlTextForTableChange = urlTextTabels + "/structID"
    $.ajax({
        url: urlTextForTableChange,
        type: "PUT",
        data: jsonDataStringForUpdate,
        contentType: conText,
        success: function () {
            emptyStruktuurideSisestusDataLabel();
            getAllTableRows();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Eroor on modifying table datas");
        }
    })
}

function emptyStruktuurideSisestusDataLabel() {
    document.getElementById("newTableName").value = "";
    document.getElementById("newColumnName").value = "";
    document.getElementById("newColumnAlias").value = "";
    document.getElementById("modifyTableId").value = "";
    document.getElementById("modifyTableName").value = "";
    document.getElementById("modifyTableColumnName").value = "";
    document.getElementById("modifyTableAliasName").value = "";
}