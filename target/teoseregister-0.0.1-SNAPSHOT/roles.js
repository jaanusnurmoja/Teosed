var allRolesList;
var allRolesJson = "http://localhost:8080/teoseregister/rest/roles";
var cntType = "application/json; charset=utf-8";


function getAllRolesAndFillTable() {
    $.getJSON(allRolesJson, function (allRoles) {
            allRolesList = allRoles;
            // <!-- see siin on tegelikult globaalse muutuja väärtustamine. Globaalset muutujat läheb vaja, et mujal sama Jsonit kasutada -->
  
            var tableBodyContent = "";
            console.log(allRoles);
            for (var i = 0; i < allRoles.length; i++) {
                tableBodyContent = tableBodyContent +
                    "<tr>" +
                    "<td class='id'>" + allRoles[i].roleId + "</td>" +
                    "<td class='role'>" + allRoles[i].roleName + "</td>" +
                    "<td class='abbr'>" + allRoles[i].eayCode + "</td>" +
                    "<td class='delete'><button type='button' onclick='deleteRole(" + allRoles[i].roleId + ")'>Delete</button>" + "</td>" +
                    "<td class='modify'><button type='button' onclick='modifyRole(" + allRoles[i].roleId + ")'>Modify</button>" + "</td>" +
                    "</tr>"
            }
            document.getElementById("roleTableBody").innerHTML = tableBodyContent;
        }
    );
}

getAllRolesAndFillTable();

function addNewRole() {
    var jsonData = {
        "roleName": document.getElementById("newRoleName").value,
        "eayCode": document.getElementById("newEayCode").value
    };
    var jsonString = JSON.stringify(jsonData);

    $.ajax({
        url: allRolesJson,
        type: "POST",
        data: jsonString,
        contentType: cntType,
        success: function () {
        	cleanValuesInTable();
            getAllRolesAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new role");
        }
    })
}

function deleteRole(roleId) {
    var jsonData = {
        "roleId": roleId
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: allRolesJson + "/delete/" + roleId,
        type: "DELETE",
        contentType: cntType,
        data: jsonDataString,
        success: function () {
            getAllRolesAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on deleting role");
        }
    })


}


function modifyRole(roleId) {
    for (var i = 0; i < allRolesList.length; i++) {
        if (allRolesList[i].roleId == roleId) {
            document.getElementById("modifyRoleId").value = roleId;
            document.getElementById("modifyRoleName").value = allRolesList[i].roleName;
            document.getElementById("modifyEayCode").value = allRolesList[i].eayCode;
        }
    }

}

function saveRoleChanges() {
    var jsonData = {
        "roleId": document.getElementById("modifyRoleId").value,
        "roleName": document.getElementById("modifyRoleName").value,
        "eayCode": document.getElementById("modifyEayCode").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    alert(jsonDataString);
    var allRolesJsonForChange = allRolesJson + "/update";
    alert(allRolesJsonForChange);
    $.ajax({
        url: allRolesJsonForChange,
        type: "PUT",
        data: jsonDataString,
        contentType: cntType,
        success: function () {
        	cleanValuesInTable()
            getAllRolesAndFillTable()
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on modifying teose tüüp")
        }
    })

}

function cleanValuesInTable() {
	document.getElementById("newRoleName").value = "";
    document.getElementById("newEayCode").value = "";
    document.getElementById("modifyRoleId").value = "";
    document.getElementById("modifyRoleName").value = "";
    document.getElementById("modifyEayCode").value = "";
}


